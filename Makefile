.PHONY: build
build:
	npm run build	

.PHONY: build-ci
build-ci:
	npm run build:ci


.PHONY: test
test:
	npx --yes concurrently --kill-others --success first --names "CDD,UNIT" -c "magenta,blue" \
		"npx --yes npm run test:cdd" \
		"npx --yes npm run test:unit"

.PHONY: test-ci
test-ci:
	rm -rf coverage
# Run the unit tests
	npm run test:unit:ci
# Run the Storybook tests
# - Credits: https://storybook.js.org/docs/writing-tests/test-runner#run-against-non-deployed-storybooks
	npx --yes concurrently --kill-others --success first --names "SB,CDD" -c "magenta,blue" \
		"npx --yes npm run cdd -- --port 6006 --ci --quiet" \
		"npx --yes wait-on tcp:6006 && npm run test:cdd:ci"
# Merge the coverage reports
	mkdir --parents coverage/merged
	cp coverage/storybook/coverage-storybook.json coverage/merged/coverage-storybook.json
	cp coverage/unit/coverage-final.json coverage/merged/coverage-unit.json
	npx nyc merge coverage/merged coverage/coverage-final.json
# Generate the coverage report
	for reporter in text text-summary lcov json-summary ; do \
		npx nyc report --reporter=$$reporter -t coverage/ --report-dir coverage/ ; \
	done